package contactarab.com.in1;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Created by Admin on 1/18/2016.
 */
public class MyPagerAdapter extends FragmentStatePagerAdapter{
    Context mContext;
    ArrayList<Fragment>fragments=new ArrayList<>();
    ArrayList<Integer>Icons=new ArrayList<>();

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    public MyPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.mContext = context;
    }

    public void addFragment(Fragment fragment,  int drawable) {
        fragments.add(fragment);
        Icons.add(drawable);
    }




    @Override
    public int getItemPosition(Object object)
    {
        return POSITION_UNCHANGED;
    }



    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }

    public View getTabView(int position) {
        View tab = LayoutInflater.from(mContext).inflate(R.layout.maintab, null);
        ImageView tabImage = (ImageView) tab.findViewById(R.id.tab_Image);
        tabImage.setBackgroundResource(Icons.get(position));
        return tab;
    }

}