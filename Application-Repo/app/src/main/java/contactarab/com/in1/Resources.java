package contactarab.com.in1;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Admin on 1/18/2016.
 */
public class Resources extends Fragment {

public Resources(){
setRetainInstance(true);
}

    View view;
    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {
     view=inflater.inflate(R.layout.resource_main,container,false);

        return view;
    }
}
